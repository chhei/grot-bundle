# N.B.: THIS REPOSITORY HAS MOVED AND IS KEPT FOR ARCHIVAL PURPOSES ONLY

I have converted the GIT repos into a Fossil repository which is now live at:

https://code.paleoearthlabs.org/GPlates.tmbundle

A GIT-based downstream mirror is available on Sourcehut under this URL:

https://git.sr.ht/~chhei/GPlates.tmbundle

This repository here still contains the older commit history/files.

Christian Heine, 2021-09-05